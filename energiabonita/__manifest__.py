{
    'name': "Odoo customizations for Energia bonita",
    'version': '12.0.0.0.1',
    'depends': ['easy_my_coop_es'],
    'author': "Coopdevs Treball SCCL",
    'website': 'https://coopdevs.org',
    'category': "Cooperative management",
    'description': """
    Odoo customizations for Energia bonita.
    """,
    "license": "AGPL-3",
    'data': [
        "views/become_company_cooperator_view.xml",
        "views/become_cooperator_view.xml",
        "views/res_company_view.xml",
        "views/subscription_request_view.xml",
    ],
}
